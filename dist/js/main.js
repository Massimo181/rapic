$(document).ready(function () {
    //     $('.owl-carousel').owlCarousel({
    //       navText: ["<img src='https://i.imgur.com/902f3NO.png'>","<img src='https://i.imgur.com/CQdUeXY.png'>"],
    //       loop:true,
    //       margin:10,
    //       dots: false,
    //       nav:true,
    //       responsive:{
    //           0:{
    //               items:1
    //           },
    //           600:{
    //               items:1
    //           },
    //           1000:{
    //               items:1
    //           }
    //       }
    //   })
    // $('#startTutor').modal('show');
    //STEP BLUE
    $('.step5').on('click', function () {
        $('.step5').nextAll().toggleClass('blue');
    });
    $('.step4').on('click', function () {
        $('.step4').nextAll().toggleClass('blue');
    });
    $('.step2').on('click', function () {
        $('.step2').nextAll().toggleClass('blue');
    });
    $('.step1').on('click', function () {
        $('.step1').nextAll().toggleClass('blue');
    });
    $('.step3').on('click', function () {
        $('.step3').nextAll().toggleClass('blue');
    });

    $('.step').on('click', function () {
        $('.step').toggleClass('blue');
        $('.point').toggleClass('blue');
        $('p').toggleClass('blue');
    });

    // $('.step').on('click', function () {
    //     $('.point').toggleClass('blue');
    // });
    //
    // $('.step').on('click', function () {
    //     $('p').toggleClass('blue');
    // });

    $('.mobile-burger').on('click', function () {
        $('.mobile-right-menu').toggleClass('opened');
        $('body').toggleClass('fixed');
    });
    $('.close-menu').on('click', function () {
        $('.mobile-right-menu').toggleClass('opened');
        $('body').toggleClass('fixed');
    });

    $('.n-bell').on('click', function () {
        $('.drop-notif').toggleClass('opened');
    });

    $('.h-avatar-drop').on('click', function () {
        $('.drop-ava-menu').toggleClass('opened');
    });

    $('.drop-wrap').on('click', function () {
        $('.menu-drop').toggleClass('opened');
    });


    $('#passChange').on('click', function () {
        $('.pass-change-block').toggleClass('open-block');
    });

    var logID = 'log',
        log = $('<div id="' + logID + '"></div>');
    $('body').append(log);
    $('[type*="radio"]').change(function () {
        var me = $(this);
        log.html(me.attr('value'));
    });

    $('.step-wrap').on('click', function () {
        $(this).toggleClass('click-r');
    });

});
//# sourceMappingURL=main.js.map
